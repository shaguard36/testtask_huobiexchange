﻿using Huobi.Console.Interfaces;
using Huobi.Net.Clients;
using CryptoExchange.Net.Authentication;
using Huobi.Console.Classes;
using Huobi.Net;
using Huobi.Net.Objects;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using Huobi.Net.Objects.Models;

namespace Huobi.Console.Classes
{
    public class HuobiExchange : IHuobiExchange
    {
        private HuobiClient HuobiClient { get; set; }

        private HuobiSocketClient HuobiSocketClient { get; set; }

        public delegate void Tick(string data);
        
        public Tick TickHandeler { get; set; }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="key"></param>
        /// <param name="secret"></param>
        public HuobiExchange(string key,string secret)
        {
            HuobiClient = new HuobiClient(new HuobiClientOptions()
            {
                ApiCredentials = new ApiCredentials(key, secret),
                LogLevel = LogLevel.Trace,
                RequestTimeout = TimeSpan.FromSeconds(60),
                
            });

            HuobiSocketClient = new HuobiSocketClient();
        }

        public IEnumerable<HuobiContractInfoResponse> ListUsdContracts()
        {
            return (IEnumerable<HuobiContractInfoResponse>)HuobiClient.UsdFuturesApi.ExchangeData.GetSwapInfoAsync().Result.Data;
        }
        public IEnumerable<HuobiContractInfoResponse> ListCoinContracts()
        {
            return (IEnumerable<HuobiContractInfoResponse>)HuobiClient.CoinFuturesApi.ExchangeData.GetContractInfoAsync().Result.Data;
        }
        public void SubscribeUsdContracts(string contract)
        {
           var z = HuobiSocketClient.UsdFuturesStreams.SubscribeToContractMarketDetailsAsync(contract, onData: data => { TickHandeler("Method: SubscribeUsdContracts "+JsonConvert.SerializeObject(data)); }).Result;
        }

        public void SubscribeCoinContracts(string contract)
        {
            var z = HuobiSocketClient.CoinFuturesStreams.SubscribeToContractMarketDetailsAsync(contract, onData: data => { TickHandeler("Method: SubscribeCoinContracts "+JsonConvert.SerializeObject(data)); }).Result;
        }
    }
}

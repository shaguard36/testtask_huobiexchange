﻿using Huobi.Console.Classes;
using Huobi.Net.Objects.Models;
using Newtonsoft.Json;
partial class  Program
{
        static void ConsoleWrite(string data)
        {
            Console.WriteLine(JsonConvert.SerializeObject(data));
        }

        static void PrintMainMenu()
        {
            Console.WriteLine("Menu press:");
            Console.WriteLine("1: - get list usd-m futures");
            Console.WriteLine("2: - get list coin-m futures");
            Console.WriteLine("3: - subscribe usd-m market data ");
            Console.WriteLine("4: - subscribe coin-m market data ");
            Console.WriteLine("0 or other: - exit");
        }

        static void start()
        {
            var he = new HuobiExchange("c50d8646-f6b57cf6-07347514-afwo04df3f", "d6796c47-d2c2111e-0094ebad-4415d")
            {
                TickHandeler = ConsoleWrite
            };

            PrintMainMenu();
            var switcher = Console.ReadLine();

            IEnumerable<HuobiContractInfoResponse> listUsd = null;
            IEnumerable<HuobiContractInfoResponse> listCoin = null;

            while (switcher != "0")
                {
                switch (switcher)
                {
                    case "1":
                        {
                            listUsd = (listUsd !=null)? listUsd: he.ListUsdContracts();
                            Console.WriteLine("List contracts usd-m");
                            foreach (var elem in listUsd)
                                Console.WriteLine(elem.Contract_code);
                        }
                        break;
                    case "2":
                        {
                            listCoin = (listCoin != null) ? listCoin : he.ListCoinContracts();
                            Console.WriteLine("List contracts coin-m");
                            foreach (var elem in listCoin)
                                Console.WriteLine(elem.Contract_code);
                        }
                        break;
                    case "3":
                        {
                            listUsd = (listUsd != null) ? listUsd : he.ListUsdContracts();
                            Console.WriteLine("enter contract");
                            var contract = Console.ReadLine();
                            if (listUsd.Any(d => d.Contract_code == contract)) { he.SubscribeUsdContracts(contract); } else  {Console.WriteLine("error not found contract");  };
                        }
                        break;
                    case "4":
                        {
                            listCoin = (listCoin != null) ? listCoin : he.ListCoinContracts();
                            Console.WriteLine("enter contract");
                            var contract= Console.ReadLine();
                            if (listCoin.Any(d => d.Contract_code == contract)) { he.SubscribeCoinContracts(contract); } else { Console.WriteLine("error not found contract"); };
                        }
                        break;
                    default:
                        break;
                }

                PrintMainMenu();
                switcher = Console.ReadLine();
            }
        }
}

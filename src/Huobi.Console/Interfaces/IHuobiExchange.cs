﻿using Huobi.Net.Objects.Models;

namespace Huobi.Console.Interfaces
{
    internal interface IHuobiExchange
    {
        public IEnumerable<HuobiContractInfoResponse> ListUsdContracts();
        public IEnumerable<HuobiContractInfoResponse> ListCoinContracts();
        public void SubscribeUsdContracts(string contract);
        public void SubscribeCoinContracts(string contract);
    }
}

﻿using CryptoExchange.Net.Objects;
using CryptoExchange.Net.Sockets;
using Huobi.Net.Objects.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Huobi.Net.Interfaces.Clients.UsdFuturesApi
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHuobiSocketClientUsdFuturesStreams : IDisposable
    {
        /// <summary>
        /// Subscribes to updates for a symbol
        /// <para><a href="https://huobiapi.github.io/docs/spot/v1/en/#market-ticker" /></para>
        /// </summary>
        /// <param name="symbol">The symbol to subscribe</param>
        /// <param name="onData">The handler for updates</param>
        /// <param name="ct">Cancellation token for closing this subscription</param>
        /// <returns>A stream subscription. This stream subscription can be used to be notified when the socket is disconnected/reconnected</returns>
        Task<CallResult<UpdateSubscription>> SubscribeToContractMarketDetailsAsync(string symbol, Action<DataEvent<HuobiContractData>> onData, CancellationToken ct = default);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Huobi.Net.Interfaces.Clients.UsdFuturesApi
{
    /// <summary>
    /// Usd Futures API endpoints
    /// </summary>
    public interface IHuobiClientUsdFuturesApi : IDisposable
    {
        /// <summary>
        /// Endpoints related to retrieving market data
        /// </summary>
        public IHuobiClientUsdFuturesApiExchangeData ExchangeData { get; }
    }
}

﻿using CryptoExchange.Net.Interfaces;
using Huobi.Net.Interfaces.Clients.CoinFuturesApi;
using Huobi.Net.Interfaces.Clients.SpotApi;
using Huobi.Net.Interfaces.Clients.UsdFuturesApi;

namespace Huobi.Net.Interfaces.Clients
{
    /// <summary>
    /// Client for accessing the Huobi websocket API. 
    /// </summary>
    public interface IHuobiSocketClient : ISocketClient
    {
        /// <summary>
        /// Spot streams
        /// </summary>
        public IHuobiSocketClientSpotStreams SpotStreams { get; }

        /// <summary>
        /// USD futures stream
        /// </summary>
        public IHuobiSocketClientUsdFuturesStreams UsdFuturesStreams { get; }

        /// <summary>
        /// Coin futures stream
        /// </summary>
        public IHuobiSocketClientCoinFuturesStreams CoinFuturesStreams { get; }
    }
}
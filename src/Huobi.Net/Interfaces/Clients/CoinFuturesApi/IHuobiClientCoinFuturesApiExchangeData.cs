﻿using CryptoExchange.Net.Objects;
using Huobi.Net.Objects.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Huobi.Net.Interfaces.Clients.CoinFuturesApi
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHuobiClientCoinFuturesApiExchangeData 
    {
        /// <summary>
        /// Get's information about the exchange including rate limits and symbol list
        /// 
        /// <para><a href="https://docs.huobigroup.com/docs/usdt_swap/v1/en/#general-query-swap-info" /></para>
        /// </summary>
        /// <param name="ct">Cancellation token</param>
        /// <returns>Exchange info</returns>
        Task<WebCallResult<IEnumerable<HuobiContractInfoResponse>>> GetContractInfoAsync(string symbol = "", string contract_type = "", string contract_code = "",CancellationToken ct = default);

        /// <summary>
        /// Gets the server time
        /// <para><a href="https://huobiapi.github.io/docs/spot/v1/en/#get-current-timestamp" /></para>
        /// </summary>
        /// <param name="ct">Cancellation token</param>
        /// <returns></returns>ы
        Task<WebCallResult<DateTime>> GetServerTimeAsync(CancellationToken ct = default);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Huobi.Net.Interfaces.Clients.CoinFuturesApi
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHuobiClientCoinFuturesApi : IDisposable
    {
        /// <summary>
        /// Endpoints related to retrieving market data
        /// </summary>
        public IHuobiClientCoinFuturesApiExchangeData ExchangeData { get; }
    }
   
}

﻿using CryptoExchange.Net.Interfaces;
using Huobi.Net.Interfaces.Clients.CoinFuturesApi;
using Huobi.Net.Interfaces.Clients.SpotApi;
using Huobi.Net.Interfaces.Clients.UsdFuturesApi;

namespace Huobi.Net.Interfaces.Clients
{
    /// <summary>
    /// Client for accessing the Huobi API. 
    /// </summary>
    public interface IHuobiClient : IRestClient
    {
        /// <summary>
        /// Spot endpoints
        /// </summary>
        IHuobiClientSpotApi SpotApi { get; }
        
        /// <summary>
        /// UsdFutures endpoints
        /// </summary>
        IHuobiClientUsdFuturesApi UsdFuturesApi { get; }

        /// <summary>
        /// CoinFutures endpoints
        /// </summary>
        IHuobiClientCoinFuturesApi CoinFuturesApi { get; }
    }
}
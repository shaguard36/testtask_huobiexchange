﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Huobi.Net.Objects.Models
{
    public class HuobiContractInfo
    {
        /// <summary>
        /// symbol	"BTC","ETH"...
        /// </summary>
        public string? Symbol {get; set;}
        /// <summary>
        ///  string Contract Code swap: "BTC-USDT"... , future: "BTC-USDT-210625" ...
        /// </summary>
        public string? Contract_code { get; set; }
        /// <summary>
        /// decimal Contract Value(USDT of one contract)   10, 100...
        /// </summary>
        public decimal Contract_size { get; set; }
        /// <summary>
        /// decimal Minimum Variation of Contract Price	0.001, 0.01...
        /// </summary>
        public decimal Price_tick { get; set; }
        /// <summary>
        /// string Settlement Date eg "1490759594752"
        /// </summary>
        public string? Settlement_date { get; set; }
        /// <summary>
        /// string Listing Date eg "20190808"
        /// </summary>
        public string? Create_date { get; set; }
        /// <summary>
        /// string delivery time（When the contract does not need to be delivered, the field value is an empty string），millesecond timestamp
        /// </summary>
        public string? Delivery_time { get; set; }

        /// <summary>
        /// int Contract Status	0: Delisting,1: Listing,2: Pending Listing,3: Suspension,4: Suspending of Listing,5: In Settlement,6: Delivering,7: Settlement Completed,8: Delivered
        /// </summary>
        public string? Contract_status { get; set; }
        /// <summary>
        /// string support margin mode cross："cross"；isolated："isolated"；all："all"
        /// </summary>
        public string? Support_margin_mode { get; set; }
        /// <summary>
        /// string contract type swap, this_week, next_week, quarter, next_quarter
        /// </summary>
        public string? Contract_type { get; set; }
        /// <summary>
        /// string pair    such as: “BTC-USDT”
        /// </summary>
        public string? Pair {get; set; }
        /// <summary>
        /// string business type futures, swap
        /// </summary>
        public string? Business_type { get;set; }
        /// <summary>
        /// delivery date, empty string when swap	such as: "20180720"
        /// </summary>
        public string? Delivery_date { get; set;}

    }

    public class HuobiContractInfoResponse : HuobiContractInfo
    {
        /// <summary>
        /// Timestamp of the data
        /// </summary>
        public DateTime Timestamp { get; set; }
        /// <summary>
        /// List of ticks for symbols
        /// </summary>
        public IEnumerable<HuobiContractInfo> data { get; set; } = Array.Empty<HuobiContractInfo>();
    }
}

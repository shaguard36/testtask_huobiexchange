﻿using CryptoExchange.Net.Converters;
using CryptoExchange.Net.Objects;
using Huobi.Net.Interfaces.Clients.CoinFuturesApi;
using Huobi.Net.Objects.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Huobi.Net.Clients.CoinFuturesApi
{
    public class HuobiClientCoinFuturesApiExchangeData : IHuobiClientCoinFuturesApiExchangeData
    {
        private const string ContractInfoEndpoint = "/api/v1/contract_contract_info";
        private const string ServerTimeEndpoint = "common/timestamp";

        private readonly HuobiClientCoinFuturesApi _baseClient;

        internal HuobiClientCoinFuturesApiExchangeData(HuobiClientCoinFuturesApi baseClient)
        {
            _baseClient = baseClient;
        }
        /// <inheritdoc />
        public async Task<WebCallResult<IEnumerable<HuobiContractInfoResponse>>> GetContractInfoAsync(string symbol = "",string contract_type="", string contract_code="",CancellationToken ct = default)
        {

            var parameters = new Dictionary<string, object>
            {
                { "symbol", symbol},
                { "contract_type", contract_type},
                { "contract_code", contract_code}

            };

            var result = await _baseClient.SendHuobiRequest<IEnumerable<HuobiContractInfoResponse>>(_baseClient.GetUrl(ContractInfoEndpoint), HttpMethod.Get, ct, parameters).ConfigureAwait(false);
             if (!result)
                 return result.AsError<IEnumerable<HuobiContractInfoResponse>>(result.Error!);
            
            return result;
        }

       

        /// <inheritdoc />
        public async Task<WebCallResult<DateTime>> GetServerTimeAsync(CancellationToken ct = default)
        {
            var result = await _baseClient.SendHuobiRequest<string>(_baseClient.GetUrl(ServerTimeEndpoint, "1"), HttpMethod.Get, ct, ignoreRatelimit: true).ConfigureAwait(false);
            if (!result)
                return result.AsError<DateTime>(result.Error!);
            var time = (DateTime)JsonConvert.DeserializeObject(result.Data, typeof(DateTime), new DateTimeConverter())!;
            return result.As(time);
        }
    }
}

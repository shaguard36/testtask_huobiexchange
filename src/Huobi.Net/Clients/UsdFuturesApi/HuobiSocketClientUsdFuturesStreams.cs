﻿using CryptoExchange.Net;
using CryptoExchange.Net.Authentication;
using CryptoExchange.Net.Logging;
using CryptoExchange.Net.Objects;
using CryptoExchange.Net.Sockets;
using Huobi.Net.Interfaces.Clients.UsdFuturesApi;
using Huobi.Net.Objects;
using Huobi.Net.Objects.Internal;
using Huobi.Net.Objects.Models;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Huobi.Net.Clients.UsdFuturesApi
{
    /// <inheritdoc />
    public class HuobiSocketClientUsdFuturesStreams : SocketApiClient, IHuobiSocketClientUsdFuturesStreams
    {
        #region fields
        private readonly string baseAddressAuthenticated;
        private readonly string baseAddressMbp;

        private readonly HuobiSocketClient _baseClient;
        private readonly Log _log;
        #endregion


        /// <inheritdoc />
        protected override AuthenticationProvider CreateAuthenticationProvider(ApiCredentials credentials)
            => new HuobiAuthenticationProvider(credentials, false);


        #region ctor
        internal HuobiSocketClientUsdFuturesStreams(Log log, HuobiSocketClient baseClient, HuobiSocketClientOptions options) : base(options, options.UsdFuturesStreamsOptions)
        {
            _log = log;
            _baseClient = baseClient;
            baseAddressAuthenticated = options.UsdFuturesStreamsOptions.BaseAddressAuthenticated;
            baseAddressMbp = options.UsdFuturesStreamsOptions.BaseAddressInrementalOrderBook;
        }

        #endregion

        /// <summary>
        /// Subscribe to Contract Market Details Async
        /// </summary>
        /// <param name="contract_code"></param>
        /// <param name="onData"></param>
        /// <param name="ct"></param>
        /// <returns></returns>
        public async Task<CallResult<UpdateSubscription>> SubscribeToContractMarketDetailsAsync(string contract_code, Action<DataEvent<HuobiContractData>> onData, CancellationToken ct = default)
        {
            var request = new HuobiSubscribeRequest(_baseClient.NextIdInternal().ToString(CultureInfo.InvariantCulture), $"market.{contract_code}.detail");
            var internalHandler = new Action<DataEvent<HuobiDataEvent<HuobiContractData>>>(data =>
            {
                data.Data.Timestamp = data.Timestamp;
                onData(data.As(data.Data.Data,contract_code));
            });
            return await _baseClient.SubscribeInternalAsync(this, request, null, false, internalHandler, ct).ConfigureAwait(false);
        }
       

       
    }
}

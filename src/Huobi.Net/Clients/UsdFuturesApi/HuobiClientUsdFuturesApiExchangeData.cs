﻿using CryptoExchange.Net.Converters;
using CryptoExchange.Net.Objects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Huobi.Net.Objects.Models;
using Huobi.Net.Interfaces.Clients.UsdFuturesApi;

namespace Huobi.Net.Clients.UsdFuturesApi
{
      /// <inheritdoc />
    public  class HuobiClientUsdFuturesApiExchangeData : IHuobiClientUsdFuturesApiExchangeData
    {
        private const string SwapInfoEndpoint = "linear-swap-api/v1/swap_contract_info";
        private const string ServerTimeEndpoint = "common/timestamp";

        private readonly HuobiClientUsdFuturesApi _baseClient;

        internal HuobiClientUsdFuturesApiExchangeData(HuobiClientUsdFuturesApi baseClient)
        {
            _baseClient = baseClient;
        }
        /// <inheritdoc />
        public async Task<WebCallResult<IEnumerable<HuobiContractInfoResponse>>> GetSwapInfoAsync (CancellationToken ct = default)
        {
         
            var parameters = new Dictionary<string, object>
            {
                { "business_type", "futures"}
            };

            var result = await _baseClient.SendHuobiRequest<IEnumerable<HuobiContractInfoResponse>>(_baseClient.GetUrl(SwapInfoEndpoint), HttpMethod.Get, ct, parameters).ConfigureAwait(false);
             if (!result)
                 return result.AsError <IEnumerable < HuobiContractInfoResponse >> (result.Error!);

             return result; 
           
        }
        /// <inheritdoc />
        public async Task<WebCallResult<DateTime>> GetServerTimeAsync(CancellationToken ct = default)
        {
            var result = await _baseClient.SendHuobiRequest<string>(_baseClient.GetUrl(ServerTimeEndpoint, "1"), HttpMethod.Get, ct, ignoreRatelimit: true).ConfigureAwait(false);
            if (!result)
                return result.AsError<DateTime>(result.Error!);
            var time = (DateTime)JsonConvert.DeserializeObject(result.Data, typeof(DateTime), new DateTimeConverter())!;
            return result.As(time);
        }

    }
}
